import {Button, Modal, Table, Space, Form, Input, message, Popconfirm } from 'antd';
import React, { useEffect, useState } from 'react';
import AgreementNew from './AgreementNew';

interface Item {
  agreementId:number, 
  link: string;
}

const AgreementPage = () => {
  const [data, setData] = useState<Item[]>([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [documentLink, setDocumentLink] = useState('');  

  const fetchItems = async () => {
    try {
      const response = await fetch('http://localhost:8081/agreement');
      const data = await response.json();
      setData(data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchItems();
    const interval = setInterval(() => {
      fetchItems();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const [visible, setVisible] = useState(false);

  const handleOpenModal = () => {
    setVisible(true);
  };

  const handleCloseModal = () => {
    setVisible(false);
  };

  const handleOpenModalDocument = (link: string) => {
    setDocumentLink(link);
    setModalVisible(true);
  };

  const handleCloseModalDocument = () => {
    setDocumentLink('');
    setModalVisible(false);
  };

  const handleFormSubmit = (values: any) => {
    console.log(values);
    handleCloseModal();
  };

  const handleDeleteData = async (agreementId: number) => {
    try {
      await fetch(`http://localhost:8081/agreement/delete/${agreementId}`, {
        method: 'DELETE',
      });
      setData(prevData => prevData.filter(data => data.agreementId !== agreementId));
      message.success('Item deleted successfully');
    } catch (error) {
      console.error(error);
      message.error('Failed to delete item');
    }
  };

  const handleUpdateData = async (agreementId: number) => {
    try {
      await fetch(`http://localhost:8081/agreement/`, {
        method: 'PUT',
      });
      setData(prevData => prevData.filter(data => data.agreementId !== agreementId));
      message.success('Item update successfully');
    } catch (error) {
      console.error(error);
      message.error('Failed to update item');
    }
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const columns = [
    {
      title: 'Fecha de inicio',
      dataIndex: 'startDate',
      key: 'start_date',
    },
    {
      title: 'Fecha Fin',
      dataIndex: 'endDate',
      key: 'end_date',
    },
    {
      title: 'Empresa',
      dataIndex: 'empresa',
      key: 'empresa',
    },
    {
      title: 'Estado',
      dataIndex: 'agStatus',
      key: 'ag_status',
      render: (isActive: boolean) => (
        <span>{isActive ? 'Activo' : 'Inactivo'}</span>
      ),
    },
    
    {
      title: 'Action',
      key: 'action',
      render: (_: any, record: Item) => (
        <Button onClick={() => handleOpenModalDocument(record.link)}>View Document</Button>
      ),
    },
      ]

  
  return( <>
  <Space style={{ marginBottom: 16 }}>
    <AgreementNew></AgreementNew>
  </Space>
  <Table dataSource={data} columns={columns} bordered />
  <Modal
        title="Document Viewer"
        visible={modalVisible}
        onCancel={handleCloseModalDocument}
        footer={null}
        destroyOnClose
      >
        {documentLink && (
          <iframe src={documentLink} width="100%" height="500px" frameBorder="0" title="Document" />
        )}
      </Modal>
  </>
  );
};

export default AgreementPage;