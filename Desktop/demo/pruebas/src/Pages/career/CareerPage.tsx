import {Button, Modal, Table, Space, Form, Input, message, Popconfirm } from 'antd';
import React, { useEffect, useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
import axios from 'axios';
import CareerNew from './CareerNew';
import CareerUpdate from './CareerUpdate';

interface Item {
  id:number, 
}

const CareerPage = () => {
  const [data, setData] = useState<Item[]>([]);

  const fetchItems = async () => {
    try {
      const response = await fetch('http://localhost:8081/carreras');
      const data = await response.json();
      setData(data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchItems();
    const interval = setInterval(() => {
      fetchItems();
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  const [open, setVisible] = useState(false);

  const handleOpenModal = () => {
    setVisible(true);
  };

  const handleCloseModal = () => {
    setVisible(false);
  };

  const handleFormSubmit = (values: any) => {
    console.log(values);
    handleCloseModal();
  };

  const handleDeleteData = async (id: number) => {
    try {
      await fetch(`http://localhost:8081/carreras/delete/${id}`, {
        method: 'DELETE',
      });
      setData(prevData => prevData.filter(data => data.id !== id));
      message.success('Item deleted successfully');
    } catch (error) {
      console.error(error);
      message.error('Failed to delete item');
    }
  };

  const handleUpdateData = async (id: number) => {
    try {
      await fetch(`http://localhost:8081/carrera/`, {
        method: 'PUT',
      });
      setData(prevData => prevData.filter(data => data.id !== id));
      message.success('Item update successfully');
    } catch (error) {
      console.error(error);
      message.error('Failed to update item');
    }
  };


  const handleCancel = () => {
    setVisible(false);
  };

  const columns = [
    {
      title: 'Nombre de carrera',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Coordinador de Carrera',
      dataIndex: 'coordinator',
      key: 'coordinator',
    },
    
    ]

  
  return( <>
  <Space style={{ marginBottom: 16 }}>
    <CareerNew></CareerNew>
  </Space>

  <Table dataSource={data} columns={columns} bordered />
  </>
  );
};

export default CareerPage;