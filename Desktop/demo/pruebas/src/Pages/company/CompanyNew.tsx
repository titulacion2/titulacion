import React, { useState } from 'react';
import { Button, Form, Input, Modal, Select } from 'antd';
import ToggleButton from '../../components/ToggleButton';
import ForeignKeyTutor from '../../components/ForeingKeyTutor';

interface FormValues {
  name: string;
  email: string;
  message: string;
}

const CompanyNew: React.FC = () => {
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();
  const [isActive, setIsActive] = useState<boolean>(true);

  const handleOpenModal = () => {
    setVisible(true);
  };

  const handleCloseModal = () => {
    setVisible(false);
  };

  const handleForeignKeyChange = (value: number) => {
    // Aquí puedes realizar acciones con el valor seleccionado
    console.log('Llave foránea seleccionada:', value);
  };

  const handleToggleChange = (value: boolean) => {
    // Aquí puedes realizar acciones con el nuevo valor
    console.log('Nuevo valor:', value);
  };

  const handleFormSubmit = (values: FormValues) => {
    console.log('Nuevo valor:', values);
    fetch('http://localhost:8081/company', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        
      },
      
      body: JSON.stringify(values),
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        handleCloseModal();
        form.resetFields();
      })
      .catch(error => {
        console.error(error);
      });
  };

  return (
    <>
    <Button type="primary" onClick={handleOpenModal}>
        New
      </Button>
      <Modal visible={visible} onCancel={handleCloseModal} footer={null}>
        <Form form={form} onFinish={handleFormSubmit}>
          <Form.Item
            name="name"
            label="Nombre de la empresa"
            rules={[{ required: true, message: 'Please enter your name' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="phone"
            label="Numero de teléfono"
            rules={[{ required: true, message: 'Please enter your email' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="contact"
            label="Contacto adicional"
            rules={[{ required: true, message: 'Please enter a message' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="coordinates"
            label="Direccion"
            rules={[{ required: true, message: 'Please enter a message' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="tutorId"
            label="tutor Empresarial"
          >
            <ForeignKeyTutor onChange={handleForeignKeyChange}/>
          </Form.Item>
          
          <Form.Item name="coStatus" label="Estado">
            <Select>
              <Select.Option value={true}>Activo</Select.Option>
              <Select.Option value={false}>Inactivo</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default CompanyNew;
